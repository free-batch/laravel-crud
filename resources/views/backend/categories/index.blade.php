<table style="width: 100%; border-collapse: collapse" border="1">
    <tr>
        <td>Name</td>
        <td>Gender</td>
        <td>Action</td>
    </tr>
    @forelse ($categories as $item)
    <tr>
        <td>{{$item->name}}</td>
        <td>{{$item->gender}}</td>
        <td></td>
    </tr>
    @empty
        <tr>
            <td colspan="3">No Record Found!</td>
        </tr>
    @endforelse
</table>