<form action="{{ route('categories.store') }}" method="POST">
    @csrf
    <label for="name">Name: </label>
    <input type="text" name="name" placeholder="Write your Sweet Name"><br>
    <label for="gender">Select Your Gender: </label>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label>
    <input type="radio" id="others" name="gender" value="others">
    <label for="others">Others</label><br>

    <button type="submit">Save</button>
</form>